import XCTest
import CSecp256k1
import keccak256
@testable import EthereumKeys

class EthereumKeysTests: XCTestCase {
    func testKey() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let ctx = secp256k1_context_create(UInt32(SECP256K1_CONTEXT_SIGN | SECP256K1_CONTEXT_VERIFY))

        let pk = [UInt8](repeating: 0, count: 64)
        
        let pubKey :UnsafeMutablePointer<secp256k1_pubkey> = UnsafeMutableRawPointer(mutating: pk).bindMemory(to: secp256k1_pubkey.self, capacity: 1)
        
        let sig = [UInt8](repeating: 0, count: 64)
        
        let signature :UnsafeMutablePointer<secp256k1_ecdsa_signature> = UnsafeMutableRawPointer(mutating: sig).bindMemory(to: secp256k1_ecdsa_signature.self, capacity: 1)
        
        
        let privKey: [UInt8] = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
        let message: [UInt8] = Array("hello world".utf8)
        
        _ = secp256k1_ec_pubkey_create(ctx!, pubKey, privKey)
        print(privKey)
        print(pk)
        let r = secp256k1_ecdsa_sign(ctx!, signature, message, privKey, nil, nil)
        print(r)
        print(sig)
        
        
        XCTAssertEqual(secp256k1_ecdsa_verify(ctx!, signature, message, pubKey), 1)
    }
    
    func testEthereumKey() {
        XCTAssertNoThrow(EthereumKey(seed: [0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46]))
    }
    
    func testEthereumKeyAccountLength() {
        let key = EthereumKey(seed: [0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46])
        let a = key?.account
        XCTAssertEqual(a?.count, 40)
        
    }
    
    
    func testEthereumKeyAccount() {
        let key = EthereumKey(seed: [0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46])
        print([0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46].reduce("", {(r,u) -> String in return r + String(format: "%02X", u)}))

        print(key?.pubKey.reduce("", {(r,u) -> String in return r + String(format: "%02X", u)}) as Any)
        let keccak = keccak256((key?.pubKey)!)
        print(keccak.reduce("", {(r,u) -> String in return r + String(format: "%02X", u)}))
        let a = key?.account
        print(a!)
        XCTAssertEqual(a?.count, 40)
        
    }
    
    
    func testSignEthRawTx() {
        guard let key = EthereumKey(seed: [0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46,0x46]) else {
            XCTAssert(false, "Problems creating key")
            return
        }
        print(key.account)
        let msgArray: [UInt8] = [0xda,0xf5,0xa7,0x79,0xae,0x97,0x2f,0x97,0x21,0x97,0x30,0x3d,0x7b,0x57,0x47,0x46,0xc7,0xef,0x83,0xea,0xda,0xc0,0xf2,0x79,0x1a,0xd2,0x3d,0xb9,0x2e,0x4c,0x8e,0x53]
        let s = key.sign(msgArray)
        let sstr = s.reduce("", { (r, u) -> String in
            return r + String(format: "%02X", u)
        })
        
        print(sstr)
        
        print(key.pubKeyStr)
        XCTAssertTrue(EthereumKey.verify(key.pubKey, msgArray, s))
        XCTAssertEqual(sstr,  "28ef61340bd939bc2195fe537567866003e1a15d3c71ff63e1590620aa63627667cbe9d8997f761aecb703304b3800ccf555c9f3dc64214b297fb1966a3b6d8300".uppercased())
    }


    func testSignedMessage() {

        guard let key = EthereumKey(seed: "B272F592BFD83F7431352E7DF424C1BAD9AC90F5F7536F36E67080ECE723B95F".toByteArray()!) else {
            XCTAssert(false, "Problems creating key")
            return
        }
        let pubKey: [UInt8] = "ded1a2a36f2b31c475fb5b8f8a3cfa597d22795f7128d6cefe78587f4c868181d5123e6e34c039a0476ff95d055a7a9db442cf754584e2b6d2a73c69ffd4ce3b".toByteArray()!
        let account = EthereumKey.account(pubKey)
        XCTAssertEqual(account, key.account)
        XCTAssertEqual(pubKey, key.pubKey)
        let msgArray: [UInt8] = "d8aebcf243d5fc8af6848c172f995226a7a153aedb842919a0325a21a5454961".toByteArray()!
        
        let s = key.sign(msgArray)
        print(s.reduce("", { (r, u) -> String in
            return r + String(format: "%02X", u)
        }))
        
        let signed: [UInt8] = "a5d3e21feff754e340441ce86ff0472c8136ee351c489adce3f4ba24ab760a736f0c8917d0bb227c70c2bb19364dc50f1fb32025131d1c87e748af641ac2370f00".toByteArray()!
        

        XCTAssertTrue(EthereumKey.verify(pubKey, msgArray, s))
        XCTAssertTrue(EthereumKey.verify(pubKey, msgArray, signed))
    }
    

    static var allTests = [
        ("testKey", testKey),
        ("testEthereumKey", testEthereumKey),
        ("testEthereumKeyAccountLength", testEthereumKeyAccountLength),
        ("testSignEthRawTx", testSignEthRawTx),
    ]
}

//acct	String	"0X95307621AC2CECFE589AC585D9FAC096DE60A9A1"
